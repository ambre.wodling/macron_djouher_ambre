package com.webPortail.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

import com.webPortail.springboot.configuration.JpaConfiguration;


@Import(JpaConfiguration.class)
@SpringBootApplication(scanBasePackages={"com.webPortail.springboot"})
public class MacronPortailWeb {

	public static void main(String[] args) {
		
		SpringApplication.run(MacronPortailWeb.class, args);
	}
}
